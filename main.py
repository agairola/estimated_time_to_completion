from train import model_train
import pandas as pd
from numpy import rint
from numpy import mean as np_mean
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import KFold, cross_val_score
import joblib
from data_preprocessing import (
    data_preprocessing,
    dataset_preprocessing,
    inverse_target_transform
)
columns_to_include = {
    'Category': 'object',
    'BU': 'object',
    'Priority': 'object',
    'createdby': 'object'}

def main(df, columns_to_include, parameters=None, path_to_model='/home/abhijit/estimated_time_to_completion/'):
    parameters = {} if parameters is None else parameters

    #data_agg = dataset_preprocessing(df, parameters)
    data_agg = df
    if 'target_thresh' not in parameters:
        parameters['target_thresh'] = 50
    X_train, X_test, y_train, y_test, column_transformer = data_preprocessing(data_agg, columns_to_include,
                                                                parameters=parameters)
    pipeline_model = model_train(X_train, y_train, column_transformer)
    joblib.dump(pipeline_model, ''.join([path_to_model,'pipeline.pkl']), compress=1)
    print(f"pipeline dumped at {''.join([path_to_model, 'pipeline.pkl'])}")
    cv = KFold(n_splits=12)
    mae = np_mean(inverse_target_transform(-1 * cross_val_score(pipeline_model,
                                        X_test,
                                        y_test,
                                        cv=cv,
                                        scoring='neg_mean_absolute_error' )
                                        ))
    print(f'Mean Absolute Error for the model is {mae}')
    print(f'Mean Absolute Error for the test set { mean_absolute_error(inverse_target_transform(pipeline_model.predict(X_test)),inverse_target_transform(y_test)) }')


def predict(X_test, columns_to_include, path_to_model='/home/abhijit/estimated_time_to_completion/pipeline.pkl'):
    pipeline_model = joblib.load(path_to_model)
    X_test = X_test[list(columns_to_include.keys())]
    preds = pipeline_model.predict(X_test)
    return rint(inverse_target_transform(preds))


if __name__ == '__main__':
    df = pd.read_csv('/home/abhijit/Downloads/log43_estimated_time_to_completion.csv')
    df['time:timestamp'] = pd.to_datetime(df['time:timestamp']).dt.tz_localize(None)
    df = df[df['Category']!='CAT_11']
    df = df[df['time_to_completion'] < 50]
    #main(df, columns_to_include)
    actual = df.iloc[5:10]['time_to_completion']
    print(predict(df.iloc[5:10], columns_to_include))
    print(actual)