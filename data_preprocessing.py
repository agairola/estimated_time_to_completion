import pandas as pd
import numpy as np
from sklearn.compose import make_column_transformer
from sklearn.preprocessing import OneHotEncoder
from sklearn.model_selection import train_test_split


def dataset_preprocessing(df, parameters):
    caseid_key = parameters['caseid_key'] if 'caseid_key' in parameters else 'case:concept:name'
    timestamp_key = parameters['timestamp_key'] if 'timestamp_key' in parameters else 'time:timestamp'
    df = pd.to_datetime(df[timestamp_key]).dt.tz_localize(None)
    case_duration = df[[caseid_key,timestamp_key]].groupby(caseid_key).agg(['min','max']). \
        diff(axis=1).reset_index().droplevel(0,axis=1). \
        rename(columns={'': caseid_key, 'max':'time_to_completion'}).drop(columns='min')
    case_duration['time_to_completion'] = round(case_duration['time_to_completion']/pd.Timedelta(1,'h'))
    df = pd.merge(df, case_duration, on=caseid_key, how='inner')
    df = df.groupby(caseid_key).first().reset_index()
    return df

def data_preprocessing(df, columns_to_include, parameters):
    timestamp_key = parameters['timestamp_key'] if 'timestamp_key' in parameters else 'time:timestamp'
    # caseid_key = parameters['caseid_key'] if 'caseid_key' in parameters else 'case:concept:name'
    target_key = parameters['target_key'] if 'target_key' in parameters else 'time_to_completion'
    
    df['month'] = df['time:timestamp'].dt.month
    df['day'] = df['time:timestamp'].dt.day
    df['day_of_week'] = df['time:timestamp'].dt.dayofweek
    
    df['month_y'] = np.sin(2 * np.pi/12 * df['month'])
    df['month_x'] = np.cos(2 * np.pi/12 * df['month'])

    df['day_y'] = np.sin(2 * np.pi/31 * df['day'])
    df['day_x'] = np.cos(2 * np.pi/31 * df['day'])

    df['day_of_week_y'] = np.sin(2 * np.pi/6 * df['day_of_week'])
    df['day_of_week_x'] = np.cos(2 * np.pi/6 * df['day_of_week'])
    df.drop(columns=['month','day','day_of_week'], inplace=True)

    cols = {col:df[col].dtype for col in df.columns}
    columns_to_inclue = [col for col in list(cols.keys()) if cols[col]=='float' or cols[col]=='object']
    X = df[list(columns_to_include.keys())]
    y = target_transform(df[target_key])
    column_trans = make_column_transformer(
                    (OneHotEncoder(), list(X.select_dtypes('object').columns)),
                    remainder='passthrough')

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33)
    target_thresh = target_transform(parameters['target_thresh']) if 'target_thresh' in parameters else y_train.quantile(0.95)
    print(f'target_thresh: {target_thresh}, train_quantile: {y_train.quantile(0.95)}')
    y_train,train_idx = y_train[y_train < target_thresh],np.asarray(y_train.index[y_train < target_thresh])
    X_train = X_train.loc[train_idx]
    y_test, test_idx = y_test[y_test < target_thresh], np.asarray(y_test.index[y_test < target_thresh])
    X_test = X_test.loc[test_idx]
    return X_train, X_test, y_train, y_test, column_trans

def target_transform(y):
    return np.log(y + 1)

def inverse_target_transform(y):
    return np.exp(y) - 1