from xgboost import XGBRegressor
from sklearn.pipeline import Pipeline

def model_train(X,y, column_trans): 
    
    #column_trans.fit_transform(X)
    #rf = RandomForestRegressor(n_jobs=-1,n_estimators=130,max_depth=12)
    xgbr = XGBRegressor(n_jobs=-1, n_estimators=105, max_depth=8)
    pipeline = Pipeline([('transformations', column_trans), ('xgbr', xgbr)])
#     params = {
#         'xgbr__n_estimators': np.arange(100,150,5),
#         'xgbr__max_depth': np.arange(3,15,1),
#         'xgbr__learning_rate': [.03, 0.05, .07,0.1]
#     }
#     model = RandomizedSearchCV(
#                 estimator = pipeline,
#                 param_distributions=params,
#                 verbose=10, n_jobs=-1, cv=5)
    pipeline.fit(X, y)
    #print(f'best score: {model.best_score_}, \n best parameters: {model.best_params_}')
    
    return pipeline